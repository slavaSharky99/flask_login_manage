import psycopg2
import psycopg2.extras

DB_SET = {
    "db_name":"hwf",
    "user":"postgres",
    "password":"postgres",
    "host":"localhost",
    "port":5432
}

def connect(settings):

    conn = psycopg2.connect(dbname=settings["db_name"], user=settings["user"],
                            password=settings["password"], host=settings["host"],
                            port=settings["port"],
                            cursor_factory=psycopg2.extras.RealDictCursor)
    return conn
def cursor(conn):

    return conn.cursor()

def get_all_acc(cursor):
    try:
        select = """
            SELECT *
            FROM "accounts"
        """
        cursor.execute(select)
        info = cursor.fetchall()
        cursor.close()
        return info
    except Exception as e:
        print (e)
def ins_acc(conn, cursor, params):
    try:
        sel = """
            SELECT *
            FROM "accounts"
            WHERE login = %s
        """
        cursor.execute(sel, (params["login"],))
        ch = cursor.fetchone()
        if ch:
            cursor.close()
            return False
        else:
            print(params)
            insert = """
                INSERT INTO "accounts" (login, password, isea,is_active,super_user)
                VALUES (%s,%s,%s,%s,%s)
                """
            if params["isEA"] > 0:
                params["isEA"] =True
            else:
                params["isEA"] = False
            cursor.execute(insert, (params["login"], params["password"],
                                    params["isEA"],False, False))
            conn.commit()
            cursor.close()
            return True
    except Exception as e:
        print(e)

def chan_pass(conn, cursor, params):
    try:
        select = """
            SELECT password
            FROM "accounts"
            WHERE id = %s
        """
        cursor.execute(select, (params["id"],))
        p = cursor.fetchone()
        if p["password"] == params["old"]:
            upd = """
                UPDATE "accounts"
                SET password = %s
                WHERE id = %s
            """
            cursor.execute(upd, (params["new"], params["id"]))
            conn.commit()
            cursor.close()
            return True
        else:
            cursor.close()
            return False
    except Exception as e:
        print (e)

def delete_acc(conn, cursor, params):

    delete = """
        DELETE FROM "accounts"
        WHERE id = %s
    """
    try:
        cursor.execute(delete, (params['id'],))
        conn.commit()
        cursor.close()
        return True
    except Exception as e:
        print(e)

def login_sys(conn,cursor,params):

    try:
        select = """
            SELECT *
            FROM "accounts"
            WHERE login = %s AND password = %s AND is_active = %s
        """
        cursor.execute(select, (params["login"], params["password"], False))
        us = cursor.fetchone()
        if us:
            upd = """
                UPDATE "accounts"
                SET is_active = %s
                WHERE id = %s
            """
            cursor.execute(upd, (True, us["id"]))
            conn.commit()
            cursor.close()
            if us["super_user"]:
                return True, True
            else:
                return True, False
        else:
            cursor.close()
            return False, False

    except Exception as e:
        print(e)

def logout_sys(conn,cursor,params):

    try:
        select = """
            SELECT *
            FROM "accounts"
            WHERE login = %s  AND is_active = %s
        """
        cursor.execute(select, (params["login"], True))
        us = cursor.fetchone()
        print(us)
        if us:
            upd = """
                UPDATE "accounts"
                SET is_active = %s
                WHERE id = %s
            """
            cursor.execute(upd, (False, us["id"]))
            conn.commit()
            cursor.close()
            return True
        else:
            cursor.close()
            return False

    except Exception as e:
        print(e)
