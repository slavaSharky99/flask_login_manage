
from flask import Flask, request, jsonify
from functools import wraps
app = Flask(__name__)

from db_tools import connect, cursor, DB_SET

from db_tools import get_all_acc, ins_acc, chan_pass, delete_acc, login_sys,\
    logout_sys

import re



import memcache
connect_ = memcache.Client(["127.0.0.1:11211"])

conn = connect(DB_SET)

users = { i["login"]:i for i in get_all_acc(cursor(conn))}

password_setting = {
    "length": 8,
    "numbers": True,
    "uppercase_letter": True,
    "lowercase_letter": True,
    "special_symbols": False
}

def check_password(password):
    if not password:
        return False
    l = password_setting["length"]
    n = password_setting["numbers"], '1-9'
    ul = password_setting["uppercase_letter"], 'A-Z'
    ll = password_setting["lowercase_letter"], 'a-z'
    ss = password_setting["special_symbols"], '\@\~\`\'\"\;\:\/\\\|\>\<\[\]\{\}\*\+\-\*\&\#\$\,\.'
    if len(password) < l:
        return False
    reg = ''
    for i in n, ul, ll, ss:

        if i[0]:
            reg += i[1]
        else:
            continue
    if re.match(r'['+reg+']', password):
        return True
    else:
        return False

@app.route("/test")
def hello():
    return "test"

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        u = connect_.get("login")

        if u in users:
            if users[u]["super_user"]:
                return f(*args, **kwargs)
            else:
                return jsonify({"Error": "You are no super User"})
        else:
            return jsonify({"Error":"You are not auth"})

    return wrap

@app.route("/api/accounts", methods=["GET", "POST"])
def get_post_acc():

    try:
        if request.method == "GET":
            accs = get_all_acc(cursor(conn))
            return jsonify(accs)
        if request.method == "POST":
            login = request.args.get('login', type=str)
            password = request.args.get('password', type=str)
            isEA = request.args.get('isEA', default=0,type=int)
            if not login:
                return jsonify("get login")
            if isEA:
                if not password:
                    password = ''
                    d = {
                        'login':login,
                        'password': password,
                        'isEA':isEA
                    }

                    if ins_acc(conn, cursor(conn), d):
                        return jsonify({"account":"Reg"})
                    else:
                        return jsonify({"account":"No Reg", "type_err":
                                        "login is Reg already"})
            if check_password(password):

                d = {
                    'login':login,
                    'password': password,
                    'isEA':isEA
                }

                if ins_acc(conn, cursor(conn), d):
                    return jsonify({"account":"Reg"})
                else:
                    return jsonify({"account":"No Reg", "type_err":
                                    "login is Reg already"})
            else:
                return jsonify({"Error":"Password uncorrect"})
        else:
            return jsonify({"method":"false"})
    except Exception as e:
        print (e)

@app.route("/api/accounts/<id>/password", methods= ["PUT"])
def  change_password(id):

    try:
        if request.method == "PUT":
            old_pass = request.args.get('old', type=str)
            new_pass = request.args.get('new', type=str)

            if check_password(password):

                d = {
                    "old":old_pass,
                    "new":new_pass,
                    "id":id
                }

                if chan_pass(conn, cursor(conn), d):

                    return jsonify({"password":"change"})
                else:
                    return jsonify({"err":"old pass is not real"})
            else:
                return jsonify({"err":"Fail password for rule"})
        else:
            return jsonify({"method":"false"})

    except Exception as e:
        print(e)

@app.route("/api/accounts/<id>", methods = ["DELETE"])
def delete_account(id):

    try:
        if request.method == "DELETE":

            if delete_acc(conn, cursor(conn), {"id":id}):
                return jsonify({"account":"Delete"})
            else:
                return jsonify({"type":"error"})
        else:
            return jsonify({"method":"false"})
    except Exception as e:
        print (e)

@app.route("/api/accounts/login", methods=["POST"])
def login():
    try:
        if request.method == "POST":
            login = request.args.get('login', type=str)
            password = request.args.get('password', type=str)

            d = {
                "login": login,
                "password":password
            }
            c = login_sys(conn, cursor(conn), d)
            if c[0]:
                users[login]["login"] = login
                users[login]["password"] = password
                users[login]["is_active"] = True

                if c[1]:
                    users[login]["super_user"] = True
                    connect_.set("login", login)
                    print (users, "A" ,users[login], "AAAAAA")
                    return jsonify({"Auth":True})
                else:
                    connect_.set("login", login)
                    users[login]["super_user"] = False
                    return jsonify({"Auth":True})

            else:
                return jsonify({"Acc":False})
        else:
            return jsonify({"method":False})
    except Exception as e:
        print(e)

@app.route("/api/accounts/logout", methods=["POST"])
def logut():
    try:
        if request.method == "POST":
            login = request.args.get('login', type=str)
            if logout_sys(conn, cursor(conn), {"login":login}):
                return jsonify({"logout": True})
            else:
                return jsonify({"logout": False})
        else:
            return jsonify({"method":"false"})
    except Exception as e:
        print (e)

@app.route("/api/accounts/password/policy", methods=["POST"])
@login_required
def set_settings_password():
    try:
        if request.method == "POST":
            length = request.args.get('length', default=8, type=int)
            numbers = request.args.get('numbers', default=1, type=int)
            upper = request.args.get("uppercase_letter", default=0, type = int)
            lower = request.args.get("lowercase_letter", default=0,type = int)
            special_symbols = request.args.get('special_symbols', default=0,type = int)

            password_setting["length"] = length
            if numbers > 0:
                password_setting["numbers"] = True
            else:
                password_setting["numbers"] = False
            if int(upper) > 0:
                password_setting["upper"] = True
            else:
                password_setting["upper"] = False
            if int(lower) > 0:
                password_setting["lower"] = True
            else:
                password_setting["lower"] = False
            if int(special_symbols) > 0:
                password_setting["special_symbols"] = True
            else:
                password_setting["special_symbols"] = False
            print (password_setting)
            return("Ok")
        else:
            return jsonify({"method":Fasle})
    except Exception as e:
        print(e)

if __name__ == "__main__":
    app.run()
